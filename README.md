<img src="http://i.imgur.com/UzC7XPe.png" alt="Helio Training" width="226" align="center"/> v1.0.0

---------------

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)

Basic promise exercises

## Instructions

```sh
# Install dependencies
yarn
or
npm install

# Test the application
yarn test
or
npm test
```

## Coding Exercise

1. Provide the necessary code in src/index.js based on the topic of Promises as discussed in class in order to get the all tests to pass.
2. Do not alter the tests or the worker helper module, all work should be done within index.js
